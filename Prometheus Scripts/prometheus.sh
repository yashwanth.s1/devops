#!/bin/bash

# Define the Node Exporter version to install
NE_VERSION="1.5.0"

# Define the servers to install Node Exporter on
SERVERS=("192.168.7.17" "192.168.7.26")

# Loop through the servers and install Node Exporter on each one
for SERVER in "${SERVERS[@]}"
do
  # SSH into the server and install Node Exporter
  ssh yashwanth.s@"$SERVER" "curl -LO https://github.com/prometheus/node_exporter/releases/download/v$NE_VERSION/node_exporter-$NE_VERSION.linux-amd64.tar.gz && tar -xvf node_exporter-$NE_VERSION.linux-amd64.tar.gz && sudo cp node_exporter-$NE_VERSION.linux-amd64/node_exporter /usr/local/bin/ && rm -rf node_exporter-$NE_VERSION.linux-amd64.tar.gz node_exporter-$NE_VERSION.linux-amd64/ && sudo -E nohup /usr/local/bin/node_exporter > logs 2>&1 &"
done

  # Print a success message
  echo "Node Exporter successfully installed and started on $SERVER"

# Prometheus Setup
# Check if Prometheus is already installed
if command -v prometheus >/dev/null 2>&1; then
  echo "Prometheus is already installed"
else
  # Download Prometheus
  wget https://github.com/prometheus/prometheus/releases/download/v2.42.0/prometheus-2.42.0.linux-amd64.tar.gz
  # Extract the binary
  tar xvfz prometheus-2.42.0.linux-amd64.tar.gz
  # Move the binary to /usr/local/bin/
  sudo mv prometheus-2.42.0.linux-amd64/prometheus /usr/local/bin/
  sudo mv prometheus-2.42.0.linux-amd64/promtool /usr/local/bin/
  sudo mv prometheus-2.42.0.linux-amd64/prometheus.* /opt/.

  # Clean up
  rm -rf prometheus-2.42.0.linux-amd64*
  chmod +x /usr/local/bin/prometheus
  echo "Prometheus installed successfully"
  fi

# Define the path to the prometheus.yml file
config_file="/opt/prometheus.yml"

# Define the path to the servers.txt file
servers_file="/opt/servers.txt"

# Read the list of servers from the file
servers=($(cat "$servers_file"))

# Loop through the list of servers and add them to the config file if they are not already present
for server in "${servers[@]}"
do
    # Check if the server is already in the config file
    if grep -q "$server" "$config_file"; then
        echo "$server is already in $config_file, skipping."
    else
        # Append the server to the targets field
        echo "Adding $server to $config_file..."
        sed -i "/- targets:/ s/\]/,'$server:9100'\]/" "$config_file"
        fi
done

# Start Prometheus to run in background
nohup prometheus > logs&
