#!/bin/bash

#CONNECT PROM TO KAFKA

# Pull the Docker image
sudo docker pull telefonica/prometheus-kafka-adapter:1.8.0

# Run the Docker container with the specified command
sudo docker run -d -e KAFKA_BROKER_LIST=192.168.7.86:9092 -e KAFKA_TOPIC=prometheus_data -e PORT=4545 --network host --name kafka-service telefonica/prometheus-kafka-adapter:1.8.0

# Add lines in prometheus.yml file.
echo   
     "remote_write: 
       - url: \"http://192.168.7.22:4545/receive\"" >> /opt/prometheus.yml

# Start Prometheus to run in background
nohup prometheus > logs&
